#!/bin/bash

set -o pipefail
set -o errexit


#
if [[ ! -f  tls-ca.key ]] && [[ ! -f tls-ca.key ]];then
 # Generate CA private key and self-signed certificate
 openssl req -x509 -newkey rsa:4096 -keyout tls-ca.key -out tls-ca.cert -days 365 -subj "/CN=MyCA/O=MyOrganization" -nodes
fi

sign_certificate_by_ca() {
  echo "=============== generate cert for $1 ================="
  openssl genrsa -out "$1.key" -aes256 -passout pass:nifi-0
  echo "end gen rsa key"
  # Generate certificate signing request (CSR) for the certificate
  openssl req -new -key "$1.key" -out "$1.csr" -subj "/CN=nifi-0/O=NIFI" -passin pass:"$1"

  # Sign the certificate with the CA private key to create the signed certificate
  openssl x509 -req -in "$1.csr" -CA tls-ca.cert -CAkey tls-ca.key -CAcreateserial -out "$1.pem" -days 365  -passin pass:"$1"
  echo "[INFO] OK end of generate cert for $1"
}


generate_jks_key_trust() {

  cat "${NIFI_HOME}/config-data/$1.pem" > /tmp/bundle.pem
  cat "${NIFI_HOME}/conf/tls/tls-ca.cert" >> /tmp/bundle.pem
  openssl pkcs12 -export  -name "$(hostname)" -certfile /tmp/bundle.pem -inkey "${NIFI_HOME}/config-data/$1.key"  -in "${NIFI_HOME}/config-data/$1.pem"  -out /tmp/bundle.p12 -passout pass:"$1" -passin pass:"$1"
  keytool -importkeystore -noprompt -srckeystore /tmp/bundle.p12 -srcstoretype pkcs12 -srcalias "$(hostname)" -destkeystore "${NIFI_HOME}/config-data/$1.keystore.jks" -deststoretype jks -destalias "$(hostname)" -srcstorepass "$1" -deststorepass "$1"  -destkeypass "$1"
  keytool -importcert -noprompt -file "${NIFI_HOME}/conf/tls/tls-ca.cert" -alias trust_chain_ca -keystore "${NIFI_HOME}/config-data/truststore.jks" -trustcacerts -deststorepass  "$1"

}

sign_certificate_by_ca "nifi-0"
generate_jks_key_trust "nifi-0"

# Cleanup intermediate files (optional)
# rm certificate.der certificate.p12 certificate.csr tls-ca.key tls-ca.cert






########

[ req ]
default_bits       = 4096
distinguished_name = req_distinguished_name
req_extensions     = req_ext
prompt = no
[ req_distinguished_name ]
countryName                = FR
stateOrProvinceName        = Paris
localityName               = MASSY
organizationName           = Carrefour
commonName                 = ae-ops.me
[ req_ext ]
subjectAltName = @alt_names
[alt_names]
DNS.1   = localhost
DNS.2   = 11ae-ops.me
#DNS.3   = plaa3kfkmag-p-kfk-5.plaa3kfkmag.xss.xpod.carrefour.com
#IP.1    = 10.23.128.234
# le contenu du fichier "req.cnf" est clui en haut
# openssl req -out preprod-apimanager.carrefour.com.csr -key private.key -nodes -config req.cnf



#       readinessProbe:
#         initialDelaySeconds: 60
#         periodSeconds: 20
#         tcpSocket:
#           port: 8443
#          exec:
#            command:
#            - bash
#            - -c
#            - |
#
#              curl -kv \
#                --cert ${NIFI_HOME}/config-data/certs/admin/crt.pem --cert-type PEM \
#                --key ${NIFI_HOME}/config-data/certs/admin/key.pem --key-type PEM \
#                https://$(hostname -f):8443/nifi-api/controller/cluster > $NIFI_BASE_DIR/data/cluster.state
#
#              STATUS=$(jq -r ".cluster.nodes[] | select((.address==\"$(hostname -f)\") or .address==\"localhost\") | .status" $NIFI_BASE_DIR/data/cluster.state)
#              if [[ ! $STATUS = "CONNECTED" ]]; then
#                echo "Node not found with CONNECTED state. Full cluster state:"
#                jq . $NIFI_BASE_DIR/data/cluster.state
#                exit 1
#              fi



https://www.weave.works/blog/helm-charts-in-kubernetes